import React, { useState, useRef } from 'react';
import delete2 from './delete.svg';
import edit from './editar.svg';
import './App.css';
import axios from 'axios';
function App() {
  /**
   * Octavio Barreto 11/03/2022
   * Creacion de variables de estados para el desarrollo del componente
   * Creacion de variables de referencias para los inputs
  */
  const [datosTabla, setdatosTabla] = useState(null);
  const [generalData, setgeneralData] = useState(null);
  const [dataSet, setdataSet] = useState(null);
  const [ataque, setataque] = useState(0);
  const [defensa, setdefensa] = useState(0);
  const [type, settype] = useState('new');
  const nombreRef = useRef();
  const ataqueRef = useRef();
  const defensaRef = useRef();
  const imagenRef = useRef();
  /**
   * Octavio Barreto 11/03/2022
   * Ejecucion de funciones principales para cargar los datos de la pantalla
   * 
  */
  React.useEffect(() => {
    // Read Api Service data  
    loadInitialData();
  }, []);
  /* ---------------------------------------------------------------------------------------------- */
  /*                   Cargar datos de pokemons consumiendo api por el metodo GET                   */
  /* ---------------------------------------------------------------------------------------------- */
  function loadInitialData() {
    axios.get('https://pokemon-pichincha.herokuapp.com/pokemons/?idAuthor=1').then(resp => {
      setgeneralData(resp.data)
      // mapeamos la respuesta para dibujar un html de la tabla
      setdatosTabla(resp.data.map((data, index) => (
        <tr key={data.id}>
          <td style={{ textAlign: "center" }}>{data.name ?? "No data"}</td>
          <td style={{ textAlign: "center" }}><img src={data.image ?? "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png"} width="75" height="auto" /></td>
          <td style={{ textAlign: "center" }}>{data.attack}</td>
          <td style={{ textAlign: "center" }}>{data.defense}</td>
          <td style={{ textAlign: "center" }}>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "update") }} className="button " ><img src={edit} className="App-logo" width="20" height="auto" /></button>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "delete") }} className="button " ><img src={delete2} className="App-logo" width="20" height="auto" /></button>
          </td>
        </tr>)))
    })
  }
  /**
   * Octavio Barreto 11/03/2022
   * Seteamos los datos a la variable de dataSet que es donde se cargaran los datos a los inputs
   * Si es tipo delete llamamos al servicio de eliminar y cargamos los datos
  */
  function getData(data, tipo) {
    if (tipo == "delete") {
      axios.delete('https://pokemon-pichincha.herokuapp.com/pokemons/' + data.id, data).then(resp => {
        loadInitialData()
        settype("new"); setdataSet({
          "name": "",
          "image": "",
          "type": "water",
          "hp": 0,
          "attack": 0,
          "defense": 0,
          "idAuthor": 1
        })
        setataque(0);
        setdefensa(0);
      });
      return;
    }
    settype(tipo);
    setdataSet(data);
    setataque(data.attack);
    setdefensa(data.defense)
  }
  /**
   * Octavio Barreto 11/03/2022
   * Funcion para realizar la busqueda del json con find ya que en el servicio no hay como hacer la busqueda por coincidencias
   * Validamos diferentes formas de busquedadel usuario y evitamos que se quede en blanco la pantalla
   * Evitamos sobrecargar la api volviendo consumir por tal motivo solo mapeamos la respuesta obtenida a primera instancia
  */
  function changeSearch(e) {
    if (e == "") {
      setdatosTabla(generalData.map((data, index) => (
        <tr key={data.id}>
          <td style={{ textAlign: "center" }}>{data.name ?? "No data"}</td>
          <td style={{ textAlign: "center" }}><img src={data.image ?? "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png"} width="75" height="auto" /></td>
          <td style={{ textAlign: "center" }}>{data.attack}</td>
          <td style={{ textAlign: "center" }}>{data.defense}</td>
          <td style={{ textAlign: "center" }}>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "update") }} className="button " ><img src={edit} className="App-logo" width="20" height="auto" /></button>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "update") }} className="button " ><img src={delete2} className="App-logo" width="20" height="auto" /></button>
          </td>
        </tr>)))
      return
    }
    let search = generalData.find(x => x.name === e);
    if (search != undefined) {
      setdatosTabla(
        <tr key={search.id}>
          <td style={{ textAlign: "center" }}>{search.name ?? "No search"}</td>
          <td style={{ textAlign: "center" }}><img src={search.image ?? "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png"} width="75" height="auto" /></td>
          <td style={{ textAlign: "center" }}>{search.attack}</td>
          <td style={{ textAlign: "center" }}>{search.defense}</td>
          <td style={{ textAlign: "center" }}>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(search, "update") }} className="button " ><img src={edit} className="App-logo" width="20" height="auto" /></button>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(search, "update") }} className="button " ><img src={delete2} className="App-logo" width="20" height="auto" /></button>
          </td>
        </tr>)
    } else {
      setdatosTabla(generalData.map((data, index) => (
        <tr key={data.id}>
          <td style={{ textAlign: "center" }}>{data.name ?? "No data"}</td>
          <td style={{ textAlign: "center" }}><img src={data.image ?? "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png"} width="75" height="auto" /></td>
          <td style={{ textAlign: "center" }}>{data.attack}</td>
          <td style={{ textAlign: "center" }}>{data.defense}</td>
          <td style={{ textAlign: "center" }}>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "update") }} className="button " ><img src={edit} className="App-logo" width="20" height="auto" /></button>
            <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { getData(data, "update") }} className="button " ><img src={delete2} className="App-logo" width="20" height="auto" /></button>
          </td>
        </tr>)))
    }
  }
  /**
   * Octavio Barreto 11/03/2022
   * Seteamos las variables de estados para identificar el numero de rango en el label
   * 
  */
  function getRange(e, tipo) {
    if (tipo == "A") {
      setataque(e.target.value);
    } else {
      setdefensa(e.target.value)
    }
  }
  /**
   * Octavio Barreto 11/03/2022
   * Gestion de datos, realizamos las validaciones de nuevo o actualizar para la llamada a la api correspondiente
   * Armamos el json correctamente para evitar que no se envien los tipos de datos como la api lo requiere
  */
  function dataManagament() {
    const dataSent = {
      "name": nombreRef.current.value,
      "image": imagenRef.current.value,
      "type": "water",
      "hp": 0,
      "attack": Number(ataqueRef.current.value),
      "defense": Number(defensaRef.current.value),
      "idAuthor": 1
    }
    if (type == "update") {
      dataSent.id = dataSet.id;
      axios.put('https://pokemon-pichincha.herokuapp.com/pokemons/' + dataSet.id, dataSent).then(resp => {
        loadInitialData()
        settype("new"); setdataSet({
          "name": "",
          "image": "",
          "type": "water",
          "hp": 0,
          "attack": 0,
          "defense": 0,
          "idAuthor": 1
        })
        setataque(0);
        setdefensa(0);
      });
    } else {
      axios.post('https://pokemon-pichincha.herokuapp.com/pokemons/', dataSent).then(resp => {
        loadInitialData()
        settype("new"); setdataSet({
          "name": "",
          "image": "",
          "type": "water",
          "hp": 0,
          "attack": 0,
          "defense": 0,
          "idAuthor": 1
        })
        setataque(0);
        setdefensa(0);
      });

    }
  }
  /**
   * Octavio Barreto 11/03/2022
   * Retornamos el html con toda la estructura del proyecto
   * 
  */
  return (
    <>
      <div className='card' style={{ marginLeft: "20%", marginRight: "20%", marginTop: "3%" }}>
        <div className="card-body">
          <div className="row">
            <div className="col-4">
              <strong>Listado de Pokemons</strong>
              <div className="input-wrapper">
                <input type="search" onChange={(e) => { changeSearch(e.target.value) }} className="input" placeholder='Buscar por nombre completo' />
                <svg xmlns="http://www.w3.org/2000/svg" class="input-icon" viewBox="0 0 20 20" fill="currentColor">
                  <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                </svg>
              </div>
            </div>
            <div className="col-7" style={{ textAlign: "right" }} >
              <br />
              {/* seteamos en 0 el json para que los inputs vuelvan a estar vacios */}
              <button className="button btn-primary" onClick={(e) => {
                settype("new"); setdataSet({
                  "name": "",
                  "image": "",
                  "type": "water",
                  "hp": 0,
                  "attack": 0,
                  "defense": 0,
                  "idAuthor": 1
                })
              }} >Nuevo</button>
            </div>
          </div>
          <br />
          <div className="card row">
            <div className="col-12">
              <table className=" table table-responsive table-hover table-bordered table-striped" style={{ width: "100% !important" }}>
                <thead>
                  <tr style={{ height: "20% !important" }}>
                    <th style={{ width: "20%", textAlign: "center" }}>Nombre</th>
                    <th style={{ width: "5%", textAlign: "center" }}>Imagen</th>
                    <th style={{ width: "15%", textAlign: "center" }}>Ataque</th>
                    <th style={{ width: "15%", textAlign: "center" }}>Defensa</th>
                    <th style={{ width: "15%", textAlign: "center" }}>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {datosTabla}
                </tbody>
              </table>
            </div>
          </div>
          <hr />
          <div className="card " style={{ marginBottom: "5%" }}>
            <div className="row">
              <div className="col-12" style={{ textAlign: "center" }}>
                <h3><strong>{type == "update" ? "Actualizar" : "Nuevo"} Pokemon</strong></h3>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <h3><strong>Nombre</strong></h3>
                {/* Cargamos como defaultValue el name que nos viene desde el json salve el caso que sea editar lo autocompleta */}
                <input type="text" defaultValue={dataSet?.name} ref={nombreRef} style={{ width: "90%" }} placeholder="" />
              </div>
              <div className="col-6">
                {/* Cargamos como defaultValue el name que nos viene desde el json salve el caso que sea editar lo autocompleta */}
                <h3><strong>Ataque {ataque}</strong></h3>
                0<input type="range" defaultValue={dataSet?.attack} ref={ataqueRef} style={{ width: "90%" }} min="0" max="100" step="0" onChange={(e) => { getRange(e, "A"); }} />100
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <h3><strong>Imagen</strong></h3>
                {/* Cargamos como defaultValue el name que nos viene desde el json salve el caso que sea editar lo autocompleta */}
                <input type="text" defaultValue={dataSet?.image} ref={imagenRef} style={{ width: "90%" }} placeholder="URL" />
              </div>
              <div className="col-6">
                {/* Cargamos como defaultValue el name que nos viene desde el json salve el caso que sea editar lo autocompleta */}
                <h3><strong>Defensa {defensa}</strong></h3>
                0<input type="range" defaultValue={dataSet?.defense} ref={defensaRef} style={{ width: "90%" }} min="0" max="100" step="0" onChange={(e) => { getRange(e, "D"); }} />100
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-6" style={{ textAlign: "right" }}>
                {/* Realizamos un operador ternario para el nombre del button */}
                <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { dataManagament(); }} className="button " >{type == "update" ? "Actualizar" : "Guardar"} </button>
              </div>
              <div className="col-6">
                <button type="button" style={{ backgroundColor: "#7e4eec", marginRight: "30px", cursor: "pointer" }} onClick={(resp) => { }} className="button " > Cancelar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
